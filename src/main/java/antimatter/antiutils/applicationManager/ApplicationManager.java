/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.applicationManager;


import antimatter.antiutils.dataTypes.Tuple;
import antimatter.antiutils.fileSystem.BasePath;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * A global "manager" fro application functionality.
 */
public class ApplicationManager {


    // ============================[  Variables  ]============================

    /**
     * The applications base path.
     */
    public static BasePath basePath;

    /**
     * A List with all {@link Method}s which should be called before the applications shuts down.
     */
    private static List<Tuple> shutdownHooks = new ArrayList<>();


    // ============================[    Logic    ]============================


    /**
     * Register the {@code executeShutdownHooks} method itself as a shutdown hook.
     */
    public static void useShutdownHooks() {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> executeShutdownHooks()));

    }

    /**
     * Register a shutdown hook.
     *
     * @param clazz The methods {@link Class}.
     * @param method The {@link Method}.
     */
    public static void registerShutdownHook(Class clazz, Method method) {

        shutdownHooks.add(new Tuple(clazz, method));

    }


    /**
     * Executes all registered shutdown hooks.
     */
    public static void executeShutdownHooks() {

        for ( Tuple tuple : shutdownHooks ) {

            Method method = (Method) tuple.getV();
            Class clazz = (Class) tuple.getK();

            try {
                method.invoke(clazz.newInstance());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }

        }

    }


    /**
     * Returns the BasePath.
     *
     * @return {@link BasePath}
     */
    public static BasePath getBasePath() {
        return basePath;
    }

}
