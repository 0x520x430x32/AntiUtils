/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.fileSystem;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.IOException;

/**
 * A representation of a json file, which can be loaded from the disk to the class,
 * or saved from the class to the disk.
 */
@Getter
@Setter
public class JsonConfigurationFile {


    // ============================[  Variables  ]============================

    /**
     * The actual {@link File} on the drive.
     */
    @JsonIgnore
    private File file;

    /**
     * The instance which holds / will hold the values.
     */
    @JsonIgnore
    private Object valuesInstance;


    // ============================[ Constructor ]============================

    public JsonConfigurationFile(File file, Object valuesInstance) {

        // _____( Set variables )_____

        setFile(file);
        setValuesInstance(valuesInstance);

    }


    // ============================[    Logic    ]============================


    /**
     * Saves the class to its json file.
     */
    public void save() throws IOException {

        // _____( File operations )_____

        if ( !getFile().isDirectory() ) {

            if ( !getFile().exists() ) {
                getFile().createNewFile();
            }

        }
        else {

            setFile( new File(getFile().getAbsolutePath() + "/" + this.getClass().getName()) );

        }

        // _____( Actual mapping / saving )_____

        ObjectMapper mapper = new ObjectMapper();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));

        mapper.writerWithDefaultPrettyPrinter().writeValue(getFile(), getValuesInstance());

    }


    /**
     * Saves the class to the specified json file.
     *
     * @param file The json file. (Will be created if not existing!)
     */
    public void save(File file) throws IOException {

        // _____( File operations )_____

        if ( !file.isDirectory() ) {

            if ( !file.exists() ) {
                file.createNewFile();
            }

        }
        else {

            setFile( new File(file.getAbsolutePath() + "/" + this.getClass().getName()) );

        }

        // _____( Actual mapping / saving )_____

        ObjectMapper mapper = new ObjectMapper();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, getValuesInstance());

    }


    /**
     * Saves {@code clazz} to the {@link File}.
     *
     * @param clazzInstance The {@link Class} to save.
     * @param file The {@link File} to save to.
     */
    public static void saveClassToFile(Object clazzInstance, File file) throws IOException, IllegalAccessException, InstantiationException {

        // _____( File operations )_____

        if ( !file.isDirectory() ) {

            if ( !file.exists() ) {
                file.createNewFile();
            }

        }
        else {

            file = new File(file.getAbsolutePath() + "/" + clazzInstance.getClass().getName());

        }

        // _____( Actual mapping / saving )_____

        ObjectMapper mapper = new ObjectMapper();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, clazzInstance);

    }


    /**
     * Loads the content of the json file into this class.
     */
    public Object load() throws IOException, IllegalAccessException, InstantiationException {

        // _____( File operations )_____

        if ( !getFile().isDirectory() ) {

            if ( !getFile().exists() ) {

                getFile().createNewFile();

                JsonConfigurationFile.saveClassToFile(getValuesInstance(), getFile());

                return load();

            }
            else {

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));

                Object loadedValuesInstance = objectMapper.readValue(getFile(), getValuesInstance().getClass());
                setValuesInstance(loadedValuesInstance);
                return loadedValuesInstance;

            }

        }
        else { return null; }

    }

}
