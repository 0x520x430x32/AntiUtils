/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.fileSystem;


import lombok.Getter;
import lombok.Setter;

import java.io.File;


/**
 * Several utility methods, to set up and maintain an applications working directory.
 */
@Getter
@Setter
public class WorkingDirectory {


    // ============================[  Variables  ]============================

    /**
     * A reference to the working dir's path on the file system.
     */
    private File workingDirFile;

    /**
     * All the directory names / paths, which should exist.
     */
    private String[] directories;


    // ============================[ Constructor ]============================

    /**
     * Set up the working directory.
     *
     * @param directories The directory names / paths, which should be created if not already existing.
     */
    public WorkingDirectory(String[] directories) {

        // _____( Set variables )_____

        setWorkingDirFile(new File(""));
        setDirectories(directories);

        // _____( Setup directory structure )_____

        setupDirectoryStructure();

    }


    // ============================[    Logic    ]============================


    /**
     * Check if all given directories exist, and create them, if they don't.
     */
    private void setupDirectoryStructure() {

        for ( String directory : getDirectories() ) {

            File toCheckDir = new File(getWorkingDirFile().getAbsolutePath() + "/" + directory);

            if ( !toCheckDir.exists() ) {
                toCheckDir.mkdirs();
            }

        }

    }

}
