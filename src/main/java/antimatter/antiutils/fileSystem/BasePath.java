/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.fileSystem;


import lombok.Getter;
import lombok.Setter;

import java.io.File;

/**
 * A representation of an Applications base path (The path where the Main class is executed from).
 */
@Getter
@Setter
public class BasePath {


    // ============================[  Variables  ]============================

    /**
     * The Main class.
     */
    private Class clazz;

    /**
     * The actual path.
     */
    private File path;


    // ============================[ Constructor ]============================

    public BasePath(Class mainClazz) {

        // _____( Set variables )_____

        setClazz(mainClazz);
        setPath(
                new File(
                        new FilePath(
                                new File(
                                        getClazz().getProtectionDomain().getCodeSource().getLocation().getPath()
                                )
                        ).getDirectories()
                )
        );

    }

}
