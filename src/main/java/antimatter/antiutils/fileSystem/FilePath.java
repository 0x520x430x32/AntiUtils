/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.fileSystem;


import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A representation of a file path.
 */
@Getter
@Setter
public class FilePath {


    // ============================[  Variables  ]============================

    /**
     * The {@link File} object instance.
     */
    private File file;

    /**
     * The files parent directories.
     */
    private String directories;

    /**
     * The file name without parent directories.
     */
    private String fileName;


    // ============================[ Constructor ]============================

    public FilePath(File file) {

        // _____( Set variables )_____

        setFile(file);

        // _____( Evaluate the file path )_____

        evaluateFilePath();

    }


    // ============================[    Logic    ]============================


    /**
     * Splits the full path into the parent directories and the files name.
     */
    private void evaluateFilePath() {

        List<String> parts = new ArrayList<>();
        parts.addAll(Arrays.asList(getFile().getAbsolutePath().split(Pattern.quote("/"))));

        String directories = "";

        String fileName = "";

        int i = 0;
        for ( String part : parts ) {

            if ( i == parts.size()-1 ) {

                fileName = part;
                break;

            }

            directories = directories + part + "/";

            i++;

        }

        setDirectories(directories);
        setFileName(fileName);

    }

}
