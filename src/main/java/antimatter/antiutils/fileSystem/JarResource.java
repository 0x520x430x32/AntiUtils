/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.fileSystem;


import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;


/**
 * Map resources inside a jar / zip file.
 */
@Getter
@Setter
public class JarResource {


    // ============================[  Variables  ]============================

    /**
     * The resource size mapping table.
     */
    private Hashtable htSizes=new Hashtable();

    /**
     * The resource content mapping table.
     */
    private Hashtable htJarContents=new Hashtable();

    /**
     * The jar file.
     */
    private File jarFile;


    // ============================[ Constructor ]============================

    /**
     * Extract resources from jar file.
     *
     * @param jarFile The jar {@link File}.
     */
    public JarResource(File jarFile) throws FileNotFoundException, IOException {

        if ( jarFile.isDirectory() & !jarFile.exists() ) { throw new FileNotFoundException(); }

        // _____( Set variables )_____

        setJarFile(jarFile);

        // ______( Start extraction )_____

        init();

    }


    // ============================[    Logic    ]============================

    /**
     * Extracts a jar resource as a blob.
     *
     * @param name The name of the resource.
     */
    public byte[] getResource(String name) {
        return (byte[])htJarContents.get(name);
    }


    /**
     * Initialize hashtables with jar file resources.
     */
    private void init() throws IOException {

        try {

            // _____( Extract sizes )_____

            ZipFile zf=new ZipFile(getJarFile().getAbsolutePath());
            Enumeration e=zf.entries();
            while (e.hasMoreElements()) {
                ZipEntry ze=(ZipEntry)e.nextElement();
                htSizes.put(ze.getName(),new Integer((int)ze.getSize()));
            }
            zf.close();

            // _____( Extract and save resources into hashtables )_____

            FileInputStream fis=new FileInputStream(getJarFile().getAbsolutePath());
            BufferedInputStream bis=new BufferedInputStream(fis);
            ZipInputStream zis=new ZipInputStream(bis);
            ZipEntry ze=null;

            while ((ze=zis.getNextEntry())!=null) {

                if (ze.isDirectory()) {
                    continue;
                }
                int size=(int)ze.getSize();
                // -1 means unknown size.
                if (size==-1) {
                    size=((Integer)htSizes.get(ze.getName())).intValue();
                }
                byte[] b=new byte[(int)size];
                int rb=0;
                int chunk=0;
                while (((int)size - rb) > 0) {
                    chunk=zis.read(b,rb,(int)size - rb);
                    if (chunk==-1) {
                        break;
                    }
                    rb+=chunk;
                }

                // _____( Add to hashtable )_____

                getHtJarContents().put(ze.getName(),b);

            }
        } catch (NullPointerException e) {
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Dump a zip entry into a string.
     *
     * @param ze The ZipEntry.
     */
    private String dumpZipEntry(ZipEntry ze) {
        StringBuffer sb=new StringBuffer();
        if (ze.isDirectory()) {
            sb.append("d ");
        } else {
            sb.append("f ");
        }
        if (ze.getMethod()==ZipEntry.STORED) {
            sb.append("stored   ");
        } else {
            sb.append("defalted ");
        }
        sb.append(ze.getName());
        sb.append("\t");
        sb.append(""+ze.getSize());
        if (ze.getMethod()==ZipEntry.DEFLATED) {
            sb.append("/"+ze.getCompressedSize());
        }
        return (sb.toString());
    }

}
