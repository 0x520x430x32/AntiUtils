/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.dataTypes;


import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;


/**
 * A Document data type, which can hold information in a HashMap "Key, Value" format.
 */
@Getter
@Setter
public class MetaData {


    // ============================[  Variables  ]============================

    /**
     * The {@link HashMap} to store the information in.
     */
    private HashMap<Object, Object> properties = new HashMap<>();


    // ============================[    Logic    ]============================

    /**
     * Gets a property.
     *
     * @param key The properties key.
     * @return The value as an {@link Object}.
     */
    public Object get(Object key) {

        if ( getProperties().containsKey(key) ) { return getProperties().get(key); }
        else { return null; }

    }


    /**
     * Sets a properties value.
     *
     * @param key The properties key.
     * @param value The new value.
     */
    public void set(Object key, Object value) {

        getProperties().put(key, value);

    }
}
