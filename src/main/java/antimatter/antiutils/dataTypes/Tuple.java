/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.dataTypes;


import lombok.Getter;
import lombok.Setter;

/**
 * A Tuple data type.
 */
@Getter
@Setter
public class Tuple {


    // ============================[  Variables  ]============================

    /**
     * The first object.
     */
    private Object K;

    /**
     * The second object.
     */
    private Object V;


    // ============================[ Constructor ]============================

    public Tuple() {}
    public Tuple(Object K, Object V) {

        setK(K);
        setV(V);

    }

}
