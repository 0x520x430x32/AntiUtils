/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.dataTypes;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * A PortRange data type, for managing a range of ports.
 */
@Getter
@Setter
public class PortRange {


    // ============================[  Variables  ]============================

    /**
     * The port to start the range on.
     */
    private int startPort;

    /**
     * The port to stop the range on.
     */
    private int stopPort;

    /**
     * A {@link List} with all the unoccupied ports.
     */
    private List<Integer> freePorts;


    // ============================[ Constructor ]============================

    public PortRange(int startPort, int stopPort) {

        // _____( Set variables )_____

        setStartPort(startPort);
        setStopPort(stopPort);

        // _____( Initialize portrange )_____

        init();

    }


    // ============================[    Logic    ]============================

    /**
     * Initialize the {@link PortRange} with the specified ports
     */
    public void init() {

        setFreePorts(new ArrayList<Integer>(getStopPort()-getStartPort()));

        for ( int i = getStartPort(); i <= getStopPort(); i++ ) {

            getFreePorts().add(i);

        }

    }


    /**
     * Gets a random port, if any are still free.
     * Note: The port get's marked as occupied automatically.
     *
     * @return The port or 0;
     */
    public int useRandomPort() {

        int port = getFreePort();

        if ( port != 0 ) {

            occupyFreePort(port);
            return port;

        } else { return 0; }

    }


    /**
     * Returns a free port from the range, without occupying it.
     * Note: return 0 if no more ports are available.
     *
     * @return The port or 0;
     */
    public int getFreePort() {

        if ( getFreePorts().size() > 0 ) {

            return getFreePorts().get(0);

        }
        else { return 0; }

    }


    /**
     * Marks a ports as occupied, by removing it from the freePorts List,
     * as long as it is free.
     *
     * @param port The port to occupy.
     */
    public void occupyFreePort(int port) {

        if ( getFreePorts().contains(port) ) {

            // _____( Remove port using indexOf -> port is Integer, but we want the index of it )_____

            getFreePorts().remove(getFreePorts().indexOf(port));

        }

    }


    /**
     * Marks an occupied port as free.
     *
     * @param port The port to mark as free.
     */
    public void freeOccupiedPort(int port) {

        if ( port >= getStartPort() & port <= getStopPort() ) {

            if ( !getFreePorts().contains(port) ) {

                getFreePorts().add(port);

            }

        }

    }


    /**
     * Checks, whether a port is free or occupied.
     *
     * @param port The port to check.
     * @return True; False.
     */
    public boolean isPortFree(int port) {

        return getFreePorts().contains(port);

    }

}
