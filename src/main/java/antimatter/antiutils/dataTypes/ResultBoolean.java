/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.dataTypes;


/**
 * A ResultBoolean dataType, which can be used to additionally add the == null check.
 */
public class ResultBoolean {


    // ============================[  Variables  ]============================

    /**
     * Whether the result is true.
     */
    private boolean isTrue = false;


    // ============================[   GET/SET   ]============================


    public boolean isTrue() {
        return this.isTrue;
    }
    public ResultBoolean setTrue() {
        this.isTrue = true; return this;
    }
    public ResultBoolean setTrue(boolean value) {
        this.isTrue = value; return this;
    }

}
