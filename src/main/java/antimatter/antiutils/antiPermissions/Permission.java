/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.antiPermissions;

import antimatter.antiutils.dataTypes.ResultBoolean;
import lombok.Getter;
import lombok.Setter;

/**
 * A representation of a permission.
 */
@Getter
@Setter
public class Permission {


    // ============================[  Variables  ]============================

    /**
     * The name of the permission.
     */
    private String name;

    /**
     * Whether the permission is granted or denied.
     */
    private ResultBoolean granted = null;


    // ============================[ Constructor ]============================

    public Permission(){}
    public Permission(String name) {

        // _____( Set variables )_____

        setName(name);

    }
    public Permission(String name, boolean granted) {

        // _____( Set variables )_____

        setName(name);
        setGranted(new ResultBoolean().setTrue(granted));

    }

    // ============================[    Logic    ]============================

    public boolean isGranted() {

        if ( getGranted() != null ) {
            return getGranted().isTrue();
        }
        else {
            return false;
        }

    }

}
