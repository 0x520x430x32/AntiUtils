/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.antiPermissions;

import antimatter.antiutils.dataTypes.ResultBoolean;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A representation of a permission node.
 */
@Getter
@Setter
public class PermissionNode extends Permission {


    // ============================[  Variables  ]============================

    /**
     * Whether to apply the nodes granted value to its subNodes.
     */
    private boolean applyToSubNodes;

    /**
     * All the nodes subNodes / subPermissions.
     */
    private HashMap<String, Permission> subNodes = new HashMap<>();


    // ============================[ Constructor ]============================

    public PermissionNode(String name) {

        // _____( Set variables )_____

        setName(name);
        //setGranted(false);
        setApplyToSubNodes(true);

    }
    public PermissionNode(String name, boolean granted) {

        // _____( Set variables )_____

        setName(name);
        //setGranted(granted);
        setApplyToSubNodes(true);

    }
    public PermissionNode(String name, boolean granted, boolean applyToSubNodes) {

        // _____( Set variables )_____

        setName(name);
        //setGranted(granted);
        setApplyToSubNodes(applyToSubNodes);

    }
    public PermissionNode(String name, boolean granted, boolean applyToSubNodes, HashMap<String, Permission> subNodes) {

        // _____( Set variables )_____

        setName(name);
        //setGranted(granted);
        setApplyToSubNodes(applyToSubNodes);
        setSubNodes(subNodes);

    }


    // ============================[    Logic    ]============================

    /**
     * Adds a {@link Permission} to the subNodes map.
     *
     * @param node The node to add.
     */
    public void addNode(Permission node) {

        if ( !getSubNodes().containsValue(node) ) {

            getSubNodes().put(node.getName(), node);

        }

    }


    /**
     * Removes a node, if it is a subNode.
     *
     * @param node The node to remove.
     */
    public void removeNode(Permission node) {

        if ( getSubNodes().containsValue(node) ) {

            getSubNodes().remove(node.getName());

        }

    }

    /**
     * Gets a {@link Permission} by its name, if it is a sub node.
     *
     * @param name The name of the node.
     * @return The {@link Permission} or null.
     */
    public Permission getPermission(String name) {

        if ( getSubNodes().containsKey(name) ) {

            return getSubNodes().get(name);

        }
        else { return null; }

    }


    /**
     * Splits a path string like "top.sub.perm" into its sections.
     *
     * @param path The path string.
     * @return The {@link String[]} array.
     */
    public String[] splitPermissionPath(String path) {

        return path.split(Pattern.quote("."));

    }


    public ResultBoolean isPermissionGranted(String path, int pathLevel) {

        System.out.println("PathLevel: " + pathLevel);

        String[] splitPath = null;
        if ( path.contains(".") ) {
            splitPath = splitPermissionPath(path);
        }

        if ( isApplyToSubNodes() ) {

            ResultBoolean result = new ResultBoolean().setTrue(isGranted());

            if ( splitPath != null ) {

                if ( getSubNodes().containsKey(splitPath[pathLevel]) ) {

                    if ( pathLevel == splitPath.length-1 ) {

                        result = new ResultBoolean().setTrue(getSubNodes().get(splitPath[pathLevel]).isGranted());

                    }
                    else {

                        if ( getSubNodes().get(splitPath[pathLevel]) instanceof PermissionNode ) {

                            return ((PermissionNode) getSubNodes().get(splitPath[pathLevel])).isPermissionGranted(path, pathLevel + 1);

                        }

                    }

                }
                else {

                    return null;

                }

            }
            else {

                if ( getSubNodes().containsKey(path) ) {

                    return new ResultBoolean().setTrue(getSubNodes().get(path).isGranted());

                }
                else {

                    return new ResultBoolean().setTrue(isGranted());

                }

            }

            return new ResultBoolean().setTrue(result.isTrue());

        }
        else {

            if ( splitPath != null ) {

                if ( getSubNodes().containsKey(splitPath[pathLevel]) ) {

                    if ( pathLevel == splitPath.length ) {

                        return new ResultBoolean().setTrue(getSubNodes().get(splitPath[pathLevel]).isGranted());

                    }
                    else {

                        if ( getSubNodes().get(splitPath[pathLevel]) instanceof PermissionNode ) {

                            return ((PermissionNode) getSubNodes().get(splitPath[pathLevel])).isPermissionGranted(path, pathLevel + 1);

                        }
                        else {

                            return new ResultBoolean().setTrue(getSubNodes().get(splitPath[pathLevel]).isGranted());

                        }

                    }

                }
                else {

                    return null;

                }

            }
            else {

                if ( getSubNodes().containsKey(path) ) {

                    return new ResultBoolean().setTrue(getSubNodes().get(path).isGranted());

                }
                else {

                    return new ResultBoolean().setTrue(isGranted());

                }

            }

            /*for ( String permissionName : getSubNodes().keySet() ) {

                System.out.println("Checking: " + permissionName);

                if ( permissionName.equals(name) ) {

                    System.out.println("Found permission: " + permissionName + " | " + new ResultBoolean().setTrue(getSubNodes().get(permissionName).isGranted()).isTrue());

                    return new ResultBoolean().setTrue(getSubNodes().get(permissionName).isGranted());

                }

                if ( getSubNodes().get(permissionName) instanceof PermissionNode ) {

                    System.out.println("Found node!");

                    ResultBoolean result = ((PermissionNode) getSubNodes().get(permissionName)).isPermissionGranted(name, recursive);

                    if ( result != null ) {

                        System.out.println("Result not null: " + result.isTrue());

                        return result;

                    }else { System.out.println("Result is null!"); }

                }

            }

            return null;*/

        }

    }

}
