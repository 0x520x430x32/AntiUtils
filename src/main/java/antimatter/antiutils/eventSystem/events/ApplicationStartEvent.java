/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.eventSystem.events;


/**
 * An Event, which gets called, when the application starts.
 */
public class ApplicationStartEvent extends Event {
}
