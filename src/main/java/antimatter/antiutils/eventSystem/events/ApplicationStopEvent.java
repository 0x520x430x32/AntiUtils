/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.eventSystem.events;


/**
 * An Event, which is called before the application exits.
 */
public class ApplicationStopEvent extends Event {
}
