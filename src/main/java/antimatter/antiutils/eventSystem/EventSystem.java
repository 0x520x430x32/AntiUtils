/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.eventSystem;


import antimatter.antiutils.dataTypes.Tuple;
import antimatter.antiutils.eventSystem.events.Event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * The global "manager" for all event related stuff.
 */
public class EventSystem {


    // ============================[  Variables  ]============================

    /**
     * All registered EventHandler classes. (Tuple - K: The superclass of the method V: the method).
     */
    private static HashMap<Class, List<Tuple>> handlers = new HashMap<>();


    // ============================[    Logic    ]============================


    /**
     * Register a {@link Class} that contains {@link EventHandler} methods.
     *
     * @param clazz The class to register.
     */
    public static void registerHandler(Class clazz) {

        // _____( Check and register @EventHandler annotated methods in class )_____

        for ( Method method : Arrays.asList(clazz.getMethods()) ) {

            EventHandler eventHandler = method.getAnnotation(EventHandler.class);

            if ( eventHandler != null ) {

                if ( handlers.get(method.getParameterTypes()[0]) == null ) {

                    handlers.put(method.getParameterTypes()[0], new ArrayList<>());
                    handlers.get(method.getParameterTypes()[0]).add(new Tuple(clazz, method));

                }
                else {

                    handlers.get(method.getParameterTypes()[0]).add(new Tuple(clazz, method));

                }

            }

        }

    }


    /**
     * Executes code of all registered EventHandlers.
     * Note: Code is run in a new {@link Thread}.
     *
     * @param event The event instance to call.
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    public static void callEvent(final Event event) {

        new Thread(() -> {
            try {
                callEventThread(event);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }).start();

    }


    /**
     * Executes code of all registered EventHandlers.
     * Note: Code is run in the current {@link Thread}.
     *
     * @param event The event instance to call.
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    public static void callEventThread(final Event event) throws IllegalAccessException, InvocationTargetException, InstantiationException {

        for ( Tuple tuple : handlers.get(event.getClass()) ) {

            Method method = (Method) tuple.getV();
            Class clazz = (Class) tuple.getK();

            method.invoke(clazz.newInstance(), event);

        }

    }

}
