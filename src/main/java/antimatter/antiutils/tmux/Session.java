/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.tmux;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


/**
 * Represents a Tmux session.
 *
 * TODO: Implement further Tmux functionality.
 */
@Getter
@Setter
public class Session {


    // ============================[  Variables  ]============================

    /**
     * The sessions name.
     */
    public String name;

    /**
     * All currently registered sessions.
     * Note: Can be refreshed with {@code refreshSessions()} method.
     */
    public static List<Session> all = new ArrayList<>();

    // ============================[ Constructor ]============================

    public Session() {

        // _____( Set variables )_____

    }


    // ============================[    Logic    ]============================

    public void refreshSessions() {}

}
