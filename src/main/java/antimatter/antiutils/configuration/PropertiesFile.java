/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.configuration;


import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import static com.sun.org.apache.bcel.internal.util.SecuritySupport.getResourceAsStream;

/**
 * A representation of a .properties file.
 */
@Getter
@Setter
public class PropertiesFile {


    // ============================[  Variables  ]============================

    /**
     * The settings.properties file instance.
     */
    private File settingsFile;

    /**
     * The {@link Properties} instance.
     */
    private Properties properties;

    /**
     * Whether the file is inside the resource dir or not.
     */
    private boolean resource;

    /**
     * A reference to the main class.
     */
    private Class mainClazz;


    // ============================[ Constructor ]============================

    public PropertiesFile(Class mainClazz, boolean resource) {

        // _____( Set variables )_____

        setProperties(new Properties());
        setResource(resource);
        setMainClazz(mainClazz);

        if ( isResource() ) {

            if ( getMainClazz().getClassLoader().getResource("settings.properties") != null ) {

                setSettingsFile(new File("settings.properties"));

            }
            else { setSettingsFile(null); }

        }
        else {

            if ( new File("settings.properties").exists() ) {

                setSettingsFile(new File("settings.properties"));

            }
            else { setSettingsFile(null); }

        }



    }
    public PropertiesFile(Class mainClazz, File file, boolean resource) {

        // _____( Set variables )_____

        setProperties(new Properties());
        setResource(resource);
        setMainClazz(mainClazz);

        if ( isResource() ) {

            if ( getMainClazz().getClassLoader().getResource(file.getAbsolutePath()) != null ) {

                setSettingsFile(file);

            }
            else { setSettingsFile(null); }

        }
        else {

            if ( file.exists() ) {

                setSettingsFile(file);

            }
            else { setSettingsFile(null); }

        }

    }


    // ============================[    Logic    ]============================


    /**
     * Loads / parses the contents of a .properties file.
     *
     * @return True; False;
     * @throws IOException
     */
    public boolean loadFile() throws IOException {

        if ( getSettingsFile() != null ) {

            if ( isResource() ) {

                try (final InputStream stream = getResourceAsStream(getSettingsFile().getName()) ) {

                    getProperties().load(stream);

                }

                return true;

            }
            else {

                try (final InputStream stream = new FileInputStream(getSettingsFile())) {

                    getProperties().load(stream);

                }

                return true;

            }

        }
        else { return false; }

    }


    /**
     * Just simplifies the call to getProperties().get().
     *
     * @param key The key to get the value from.
     * @return The {@link Object} or {@code null}.
     */
    public Object get(Object key) {

        return getProperties().get(key);

    }

}
