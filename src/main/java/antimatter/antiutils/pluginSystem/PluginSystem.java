/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.pluginSystem;


import antimatter.antiutils.dataTypes.MetaData;
import antimatter.antiutils.fileSystem.JarResource;
import antimatter.antiutils.pluginSystem.exceptions.PluginJsonFileNotFoundException;
import antimatter.antiutils.pluginSystem.exceptions.PluginJsonFileParseException;
import antimatter.antiutils.pluginSystem.exceptions.PluginMainClassNotFoundException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

/**
 * The global "manager" for all plugin related stuff.
 */
public class PluginSystem {


    // ============================[  Variables  ]============================

    /**
     * All registered classes. (MetaData: The plugin metadata; Class: The plugins main class).
     */
    public static HashMap<MetaData, Class> plugins = new HashMap<>();

    /**
     * The directory to load the plugins from.
     */
    private static File pluginDirectory = new File("plugins");


    // ============================[    Logic    ]============================


    /**
     * Loads all plugins from the default "plugins" directory.
     */
    public static void loadPlugins() throws PluginMainClassNotFoundException, PluginJsonFileNotFoundException, PluginJsonFileParseException {

        // _____( Handle the files )_____

        handlePluginFiles();

    }


    /**
     * Loads all plugins from the given {@code pluginDir} path, if that exists.
     *
     * @param pluginDir The custom directory.
     */
    public static void loadPlugins(File pluginDir) throws PluginMainClassNotFoundException, PluginJsonFileNotFoundException, PluginJsonFileParseException {

        // _____( Set variables )_____

        if ( pluginDirectory.exists() ) { pluginDirectory = pluginDir; }

        // _____( Handle the files )_____

        handlePluginFiles();

    }


    /**
     * Registers the plugins if they are valid.
     */
    public static void handlePluginFiles() throws PluginMainClassNotFoundException, PluginJsonFileNotFoundException, PluginJsonFileParseException {

        for ( File pluginFile : FileUtils.listFiles(pluginDirectory, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)) {

            if ( pluginFile.getAbsolutePath().endsWith(".jar") ) {

                JarResource jarResource;
                try {
                    jarResource = new JarResource(pluginFile);
                } catch (IOException e) {
                    throw new PluginJsonFileNotFoundException("Could not find antiplugin.json file for plugin: " + pluginFile.getName());
                }

                String antiPluginJsonString = new String(jarResource.getResource("antiplugin.json"), StandardCharsets.UTF_8);

                JSONParser parser = new JSONParser();
                JSONObject jsonObject;
                try {
                     jsonObject = (JSONObject) parser.parse(antiPluginJsonString);
                } catch (ParseException e) {
                    throw new PluginJsonFileParseException("Could not parse antiplugin.json file for plugin: " + pluginFile.getName());
                }

                if ( jsonObject != null ) {

                    MetaData pluginMetadata = new MetaData();

                    for ( Object key : jsonObject.keySet().toArray() ) {

                        pluginMetadata.set(key, jsonObject.get(key));

                    }

                    Class pluginClass;
                    try {

                        URL url = pluginFile.toURI().toURL();
                        URL[] urls = new URL[]{url};

                        ClassLoader classLoader = new URLClassLoader(urls);
                        pluginClass = classLoader.loadClass((String) pluginMetadata.get("mainClass"));

                    }
                    catch (Exception ex) { throw new PluginMainClassNotFoundException("Could not find main class: " + pluginMetadata.get("mainClass")); }

                    if ( pluginClass != null ) {
                        plugins.put(pluginMetadata, pluginClass);
                    }

                }

            }

        }

    }


    /**
     * Executes all start routines.
     */
    public static void enablePlugins() throws IllegalAccessException, InstantiationException {

        for ( MetaData metaData : plugins.keySet() ) {

            AntiPlugin antiPlugin = (AntiPlugin) plugins.get(metaData).newInstance();

            antiPlugin.onEnable();

        }

    }

    /**
     * Executes all stop routines-
     */
    public static void disablePlugins() throws IllegalAccessException, InstantiationException {

        for ( MetaData metaData : plugins.keySet() ) {

            AntiPlugin antiPlugin = (AntiPlugin) plugins.get(metaData).newInstance();

            antiPlugin.onDisable();

        }

    }

}
