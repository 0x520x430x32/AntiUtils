/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.pluginSystem.exceptions;


/**
 * Raised when the main class of a plugin could not be found.
 */
public class PluginMainClassNotFoundException extends Exception {

    public PluginMainClassNotFoundException(String message) {
        super(message);
    }

}
