/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.pluginSystem.exceptions;


/**
 * Raised when the antiplugin.json file could not be parsed correctly.
 */
public class PluginJsonFileParseException extends Exception {

    public PluginJsonFileParseException(String message) { super(message); }

}
