/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.pluginSystem.exceptions;


/**
 * Raised when the antiplugin.json file could not be extracted from the jar file.
 */
public class PluginJsonFileNotFoundException extends Exception {

    public PluginJsonFileNotFoundException(String message) {
        super(message);
    }

}
