/*
 * Copyright (c) 2017 - 0x520x430x32.
 */

package antimatter.antiutils.pluginSystem;

/**
 * Represents the "canvas" for a plugin.
 */
public class AntiPlugin {


    /**
     * Execute code, which gets called, after all plugins have been loaded.
     */
    public void onEnable() {}


    /**
     * Execute code, which gets called, before all plugins get unloaded.
     */
    public void onDisable() {}

}
